/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"eventCode": "eventCode",
		"placeholderCode": "placeholderCode",
		"scale": "scale",
		"campaignId": "campaignId",
		"campaignPlaceholderId": "campaignPlaceholderId",
		"imageURL": "imageURL",
		"imageIndex": "imageIndex",
		"destinationURL": "destinationURL",
		"targetURL": "targetURL",
		"bannerDescription": "bannerDescription",
		"bannerTitle": "bannerTitle",
		"callToActionButtonLabel": "callToActionButtonLabel",
		"showCloseIcon": "showCloseIcon",
		"callToActionTargetURL": "callToActionTargetURL",
		"showReadLaterButton": "showReadLaterButton",
	};

	Object.freeze(mappings);

	var typings = {
		"eventCode": "string",
		"placeholderCode": "string",
		"scale": "string",
		"campaignId": "string",
		"campaignPlaceholderId": "string",
		"imageURL": "string",
		"imageIndex": "string",
		"destinationURL": "string",
		"targetURL": "string",
		"bannerDescription": "string",
		"bannerTitle": "string",
		"callToActionButtonLabel": "string",
		"showCloseIcon": "string",
		"callToActionTargetURL": "string",
		"showReadLaterButton": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"eventCode",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "CampaignEngine",
		tableName: "Campaigns"
	};

	Object.freeze(config);

	return config;
})