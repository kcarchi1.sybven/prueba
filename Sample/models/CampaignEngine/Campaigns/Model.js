/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "Campaigns", "objectService" : "CampaignEngine"};

    var setterFunctions = {
        eventCode: function(val, state) {
            context["field"] = "eventCode";
            context["metadata"] = (objectMetadata ? objectMetadata["eventCode"] : null);
            state['eventCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        placeholderCode: function(val, state) {
            context["field"] = "placeholderCode";
            context["metadata"] = (objectMetadata ? objectMetadata["placeholderCode"] : null);
            state['placeholderCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        scale: function(val, state) {
            context["field"] = "scale";
            context["metadata"] = (objectMetadata ? objectMetadata["scale"] : null);
            state['scale'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        campaignId: function(val, state) {
            context["field"] = "campaignId";
            context["metadata"] = (objectMetadata ? objectMetadata["campaignId"] : null);
            state['campaignId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        campaignPlaceholderId: function(val, state) {
            context["field"] = "campaignPlaceholderId";
            context["metadata"] = (objectMetadata ? objectMetadata["campaignPlaceholderId"] : null);
            state['campaignPlaceholderId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        imageURL: function(val, state) {
            context["field"] = "imageURL";
            context["metadata"] = (objectMetadata ? objectMetadata["imageURL"] : null);
            state['imageURL'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        imageIndex: function(val, state) {
            context["field"] = "imageIndex";
            context["metadata"] = (objectMetadata ? objectMetadata["imageIndex"] : null);
            state['imageIndex'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        destinationURL: function(val, state) {
            context["field"] = "destinationURL";
            context["metadata"] = (objectMetadata ? objectMetadata["destinationURL"] : null);
            state['destinationURL'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        targetURL: function(val, state) {
            context["field"] = "targetURL";
            context["metadata"] = (objectMetadata ? objectMetadata["targetURL"] : null);
            state['targetURL'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        bannerDescription: function(val, state) {
            context["field"] = "bannerDescription";
            context["metadata"] = (objectMetadata ? objectMetadata["bannerDescription"] : null);
            state['bannerDescription'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        bannerTitle: function(val, state) {
            context["field"] = "bannerTitle";
            context["metadata"] = (objectMetadata ? objectMetadata["bannerTitle"] : null);
            state['bannerTitle'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        callToActionButtonLabel: function(val, state) {
            context["field"] = "callToActionButtonLabel";
            context["metadata"] = (objectMetadata ? objectMetadata["callToActionButtonLabel"] : null);
            state['callToActionButtonLabel'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        showCloseIcon: function(val, state) {
            context["field"] = "showCloseIcon";
            context["metadata"] = (objectMetadata ? objectMetadata["showCloseIcon"] : null);
            state['showCloseIcon'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        callToActionTargetURL: function(val, state) {
            context["field"] = "callToActionTargetURL";
            context["metadata"] = (objectMetadata ? objectMetadata["callToActionTargetURL"] : null);
            state['callToActionTargetURL'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        showReadLaterButton: function(val, state) {
            context["field"] = "showReadLaterButton";
            context["metadata"] = (objectMetadata ? objectMetadata["showReadLaterButton"] : null);
            state['showReadLaterButton'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function Campaigns(defaultValues) {
        var privateState = {};
        context["field"] = "eventCode";
        context["metadata"] = (objectMetadata ? objectMetadata["eventCode"] : null);
        privateState.eventCode = defaultValues ?
            (defaultValues["eventCode"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["eventCode"], context) :
                null) :
            null;

        context["field"] = "placeholderCode";
        context["metadata"] = (objectMetadata ? objectMetadata["placeholderCode"] : null);
        privateState.placeholderCode = defaultValues ?
            (defaultValues["placeholderCode"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["placeholderCode"], context) :
                null) :
            null;

        context["field"] = "scale";
        context["metadata"] = (objectMetadata ? objectMetadata["scale"] : null);
        privateState.scale = defaultValues ?
            (defaultValues["scale"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["scale"], context) :
                null) :
            null;

        context["field"] = "campaignId";
        context["metadata"] = (objectMetadata ? objectMetadata["campaignId"] : null);
        privateState.campaignId = defaultValues ?
            (defaultValues["campaignId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["campaignId"], context) :
                null) :
            null;

        context["field"] = "campaignPlaceholderId";
        context["metadata"] = (objectMetadata ? objectMetadata["campaignPlaceholderId"] : null);
        privateState.campaignPlaceholderId = defaultValues ?
            (defaultValues["campaignPlaceholderId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["campaignPlaceholderId"], context) :
                null) :
            null;

        context["field"] = "imageURL";
        context["metadata"] = (objectMetadata ? objectMetadata["imageURL"] : null);
        privateState.imageURL = defaultValues ?
            (defaultValues["imageURL"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["imageURL"], context) :
                null) :
            null;

        context["field"] = "imageIndex";
        context["metadata"] = (objectMetadata ? objectMetadata["imageIndex"] : null);
        privateState.imageIndex = defaultValues ?
            (defaultValues["imageIndex"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["imageIndex"], context) :
                null) :
            null;

        context["field"] = "destinationURL";
        context["metadata"] = (objectMetadata ? objectMetadata["destinationURL"] : null);
        privateState.destinationURL = defaultValues ?
            (defaultValues["destinationURL"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["destinationURL"], context) :
                null) :
            null;

        context["field"] = "targetURL";
        context["metadata"] = (objectMetadata ? objectMetadata["targetURL"] : null);
        privateState.targetURL = defaultValues ?
            (defaultValues["targetURL"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["targetURL"], context) :
                null) :
            null;

        context["field"] = "bannerDescription";
        context["metadata"] = (objectMetadata ? objectMetadata["bannerDescription"] : null);
        privateState.bannerDescription = defaultValues ?
            (defaultValues["bannerDescription"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["bannerDescription"], context) :
                null) :
            null;

        context["field"] = "bannerTitle";
        context["metadata"] = (objectMetadata ? objectMetadata["bannerTitle"] : null);
        privateState.bannerTitle = defaultValues ?
            (defaultValues["bannerTitle"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["bannerTitle"], context) :
                null) :
            null;

        context["field"] = "callToActionButtonLabel";
        context["metadata"] = (objectMetadata ? objectMetadata["callToActionButtonLabel"] : null);
        privateState.callToActionButtonLabel = defaultValues ?
            (defaultValues["callToActionButtonLabel"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["callToActionButtonLabel"], context) :
                null) :
            null;

        context["field"] = "showCloseIcon";
        context["metadata"] = (objectMetadata ? objectMetadata["showCloseIcon"] : null);
        privateState.showCloseIcon = defaultValues ?
            (defaultValues["showCloseIcon"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["showCloseIcon"], context) :
                null) :
            null;

        context["field"] = "callToActionTargetURL";
        context["metadata"] = (objectMetadata ? objectMetadata["callToActionTargetURL"] : null);
        privateState.callToActionTargetURL = defaultValues ?
            (defaultValues["callToActionTargetURL"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["callToActionTargetURL"], context) :
                null) :
            null;

        context["field"] = "showReadLaterButton";
        context["metadata"] = (objectMetadata ? objectMetadata["showReadLaterButton"] : null);
        privateState.showReadLaterButton = defaultValues ?
            (defaultValues["showReadLaterButton"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["showReadLaterButton"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "eventCode": {
                get: function() {
                    context["field"] = "eventCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["eventCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.eventCode, context);
                },
                set: function(val) {
                    setterFunctions['eventCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "placeholderCode": {
                get: function() {
                    context["field"] = "placeholderCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["placeholderCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.placeholderCode, context);
                },
                set: function(val) {
                    setterFunctions['placeholderCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "scale": {
                get: function() {
                    context["field"] = "scale";
                    context["metadata"] = (objectMetadata ? objectMetadata["scale"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.scale, context);
                },
                set: function(val) {
                    setterFunctions['scale'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "campaignId": {
                get: function() {
                    context["field"] = "campaignId";
                    context["metadata"] = (objectMetadata ? objectMetadata["campaignId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.campaignId, context);
                },
                set: function(val) {
                    setterFunctions['campaignId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "campaignPlaceholderId": {
                get: function() {
                    context["field"] = "campaignPlaceholderId";
                    context["metadata"] = (objectMetadata ? objectMetadata["campaignPlaceholderId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.campaignPlaceholderId, context);
                },
                set: function(val) {
                    setterFunctions['campaignPlaceholderId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "imageURL": {
                get: function() {
                    context["field"] = "imageURL";
                    context["metadata"] = (objectMetadata ? objectMetadata["imageURL"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.imageURL, context);
                },
                set: function(val) {
                    setterFunctions['imageURL'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "imageIndex": {
                get: function() {
                    context["field"] = "imageIndex";
                    context["metadata"] = (objectMetadata ? objectMetadata["imageIndex"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.imageIndex, context);
                },
                set: function(val) {
                    setterFunctions['imageIndex'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "destinationURL": {
                get: function() {
                    context["field"] = "destinationURL";
                    context["metadata"] = (objectMetadata ? objectMetadata["destinationURL"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.destinationURL, context);
                },
                set: function(val) {
                    setterFunctions['destinationURL'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "targetURL": {
                get: function() {
                    context["field"] = "targetURL";
                    context["metadata"] = (objectMetadata ? objectMetadata["targetURL"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.targetURL, context);
                },
                set: function(val) {
                    setterFunctions['targetURL'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "bannerDescription": {
                get: function() {
                    context["field"] = "bannerDescription";
                    context["metadata"] = (objectMetadata ? objectMetadata["bannerDescription"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.bannerDescription, context);
                },
                set: function(val) {
                    setterFunctions['bannerDescription'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "bannerTitle": {
                get: function() {
                    context["field"] = "bannerTitle";
                    context["metadata"] = (objectMetadata ? objectMetadata["bannerTitle"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.bannerTitle, context);
                },
                set: function(val) {
                    setterFunctions['bannerTitle'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "callToActionButtonLabel": {
                get: function() {
                    context["field"] = "callToActionButtonLabel";
                    context["metadata"] = (objectMetadata ? objectMetadata["callToActionButtonLabel"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.callToActionButtonLabel, context);
                },
                set: function(val) {
                    setterFunctions['callToActionButtonLabel'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "showCloseIcon": {
                get: function() {
                    context["field"] = "showCloseIcon";
                    context["metadata"] = (objectMetadata ? objectMetadata["showCloseIcon"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.showCloseIcon, context);
                },
                set: function(val) {
                    setterFunctions['showCloseIcon'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "callToActionTargetURL": {
                get: function() {
                    context["field"] = "callToActionTargetURL";
                    context["metadata"] = (objectMetadata ? objectMetadata["callToActionTargetURL"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.callToActionTargetURL, context);
                },
                set: function(val) {
                    setterFunctions['callToActionTargetURL'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "showReadLaterButton": {
                get: function() {
                    context["field"] = "showReadLaterButton";
                    context["metadata"] = (objectMetadata ? objectMetadata["showReadLaterButton"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.showReadLaterButton, context);
                },
                set: function(val) {
                    setterFunctions['showReadLaterButton'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.eventCode = value ? (value["eventCode"] ? value["eventCode"] : null) : null;
            privateState.placeholderCode = value ? (value["placeholderCode"] ? value["placeholderCode"] : null) : null;
            privateState.scale = value ? (value["scale"] ? value["scale"] : null) : null;
            privateState.campaignId = value ? (value["campaignId"] ? value["campaignId"] : null) : null;
            privateState.campaignPlaceholderId = value ? (value["campaignPlaceholderId"] ? value["campaignPlaceholderId"] : null) : null;
            privateState.imageURL = value ? (value["imageURL"] ? value["imageURL"] : null) : null;
            privateState.imageIndex = value ? (value["imageIndex"] ? value["imageIndex"] : null) : null;
            privateState.destinationURL = value ? (value["destinationURL"] ? value["destinationURL"] : null) : null;
            privateState.targetURL = value ? (value["targetURL"] ? value["targetURL"] : null) : null;
            privateState.bannerDescription = value ? (value["bannerDescription"] ? value["bannerDescription"] : null) : null;
            privateState.bannerTitle = value ? (value["bannerTitle"] ? value["bannerTitle"] : null) : null;
            privateState.callToActionButtonLabel = value ? (value["callToActionButtonLabel"] ? value["callToActionButtonLabel"] : null) : null;
            privateState.showCloseIcon = value ? (value["showCloseIcon"] ? value["showCloseIcon"] : null) : null;
            privateState.callToActionTargetURL = value ? (value["callToActionTargetURL"] ? value["callToActionTargetURL"] : null) : null;
            privateState.showReadLaterButton = value ? (value["showReadLaterButton"] ? value["showReadLaterButton"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(Campaigns);

    //Create new class level validator object
    BaseModel.Validator.call(Campaigns);

    var registerValidatorBackup = Campaigns.registerValidator;

    Campaigns.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(Campaigns.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    //For Operation 'getCampaign' with service id 'getCampaigns1236'
     Campaigns.getCampaign = function(params, onCompletion){
        return Campaigns.customVerb('getCampaign', params, onCompletion);
     };

    //For Operation 'getPreLoginCampaigns' with service id 'getCampaigns7866'
     Campaigns.getPreLoginCampaigns = function(params, onCompletion){
        return Campaigns.customVerb('getPreLoginCampaigns', params, onCompletion);
     };

    var relations = [];

    Campaigns.relations = relations;

    Campaigns.prototype.isValid = function() {
        return Campaigns.isValid(this);
    };

    Campaigns.prototype.objModelName = "Campaigns";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    Campaigns.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("CampaignEngine", "Campaigns", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    Campaigns.clone = function(objectToClone) {
        var clonedObj = new Campaigns();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return Campaigns;
});